This is an example/starter binder with CPython 2.7 (yes you really should
upgrade to 3.x already), numpy, scipy and matplotlib. You can test it with the
link below.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.flux.utah.edu%2Falex_orange%2Fbinder-example-cpython-2.7/master)
